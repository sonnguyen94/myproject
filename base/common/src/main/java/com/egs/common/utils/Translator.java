package com.egs.common.utils;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class Translator {

	private static ResourceBundleMessageSource messageSource;

	@Autowired
	Translator(ResourceBundleMessageSource messageSource) {
		Translator.messageSource = messageSource;
	}

	/**
	 * Translate message
	 * 
	 * @param msg
	 * @param params
	 * @return
	 */
	public static String translateMessage(String msg, String... params) {
		Locale locale = LocaleContextHolder.getLocale();
		String message = messageSource.getMessage(msg, params, locale);

		return message;
	}
}
