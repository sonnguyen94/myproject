package com.egs.common.logging.strategy;

import org.springframework.boot.logging.LogLevel;

import com.egs.common.logging.LogService;

/**
 * Base implementation for all log strategies. New log strategy must be derived
 * from this class.
 * 
 * @author Son Nguyen
 *
 * @param <T>
 */
public abstract class LogStrategyBase<T> implements LogStrategy<T> {

	/**
	 * the actual logger (e.g. Logger from log4j) will be initialized in concrete
	 * class
	 */
	protected T logger;

	/**
	 *
	 * @param level
	 * @param methodName
	 * @param separator
	 * @param methodParams
	 */
	protected abstract void log(LogLevel level, String methodName, String separator, Object... methodParams);

	/**
	 *
	 * @param level
	 * @param methodName
	 * @param separator
	 * @param messageFormat
	 * @param messageParams
	 */
	protected abstract void logMessage(LogLevel level, String methodName, String separator, String messageFormat,
			Object... messageParams);

	/**
	 *
	 * @param level
	 * @param methodName
	 * @param separator
	 * @param t
	 */
	protected abstract void logThrowable(LogLevel level, String methodName, String separator, Throwable t);

	@Override
	public T getLogger() {
		return logger;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void entering(String methodName, Object... methodParams) {
		log(LogLevel.TRACE, methodName, LogService.METHOD_ENTER, methodParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enteringMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.TRACE, methodName, LogService.METHOD_ENTER, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exiting(String methodName) {
		log(LogLevel.TRACE, methodName, LogService.METHOD_EXIT);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exitingMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.TRACE, methodName, LogService.METHOD_EXIT, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug(String methodName, Object... methodParams) {
		log(LogLevel.DEBUG, methodName, LogService.METHOD_SEPARATOR, methodParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debugMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.DEBUG, methodName, LogService.METHOD_SEPARATOR, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debugThrowable(String methodName, Throwable t) {
		logThrowable(LogLevel.DEBUG, methodName, LogService.METHOD_SEPARATOR, t);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info(String methodName, Object... methodParams) {
		log(LogLevel.INFO, methodName, LogService.METHOD_SEPARATOR, methodParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void infoMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.INFO, methodName, LogService.METHOD_SEPARATOR, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void infoThrowable(String methodName, Throwable t) {
		logThrowable(LogLevel.INFO, methodName, LogService.METHOD_SEPARATOR, t);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn(String methodName, Object... methodParams) {
		log(LogLevel.WARN, methodName, LogService.METHOD_SEPARATOR, methodParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warnMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.WARN, methodName, LogService.METHOD_SEPARATOR, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warnThrowable(String methodName, Throwable t) {
		logThrowable(LogLevel.WARN, methodName, LogService.METHOD_SEPARATOR, t);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error(String methodName, Object... methodParams) {
		log(LogLevel.ERROR, methodName, LogService.METHOD_SEPARATOR, methodParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void errorMessage(String methodName, String messageFormat, Object... messageParams) {
		logMessage(LogLevel.ERROR, methodName, LogService.METHOD_SEPARATOR, messageFormat, messageParams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void errorThrowable(String methodName, Throwable t) {
		logThrowable(LogLevel.ERROR, methodName, LogService.METHOD_SEPARATOR, t);
	}
}
