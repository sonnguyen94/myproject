package com.egs.common.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Son Nguyen
 *
 */
public final class TimeUtils {

	public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

	public static final String DATE_FORMAT = "yyyy-MM-dd";

	private TimeUtils() {
	}

	/**
	 * @return the current timestamp object
	 */
	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * @param ts
	 *            for this day the beginning is determined, if <b>null</b> the
	 *            current day is used
	 * @return the beginning of the day as <i>Timestamp</i>
	 */
	public static Timestamp getDayStart(Timestamp ts) {
		Timestamp now = new Timestamp(ts == null ? System.currentTimeMillis() : ts.getTime());
		Calendar cal = Calendar.getInstance();

		cal.setTime(now);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return new Timestamp(cal.getTimeInMillis());
	}

	/**
	 * @param ts
	 *            for this day the end is determined, if <b>null</b> the current day
	 *            is used
	 * @return the end of the day (i.e. the last millisecond) as <i>Timestamp</i>
	 */
	public static Timestamp getDayEnd(Timestamp ts) {
		return new Timestamp(getDayStart(ts).getTime() + TimeUnit.DAYS.toMillis(1) - 1);
	}

	/**
	 * Convert Timestamp to string
	 * 
	 * @param ts
	 * @param pattern
	 * @return
	 */
	public static String convertDateTime(Timestamp ts, String pattern) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(ts);
	}

	/**
	 * Get date time format
	 * 
	 * @param ts
	 * @return e.g: 2019-01-30 12:00
	 */
	public static String getDataTimeFormat(Timestamp ts) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		return simpleDateFormat.format(ts);
	}

	/**
	 * Get date format
	 * 
	 * @param ts
	 * @return e.g: 2019-01-30
	 */
	public static String getDateFormat(Timestamp ts) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		return simpleDateFormat.format(ts);
	}

	/**
	 * Convert string to timestamp
	 * 
	 * @param ts
	 * @param pattern
	 * @return
	 */
	public static Timestamp parseDateTime(String ts, String pattern) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = simpleDateFormat.parse(ts);
		return new Timestamp(date.getTime());
	}

	/**
	 * Parse date time as a string to Timestamp
	 * 
	 * @param ts
	 *            (e.g: 2019-01-30 12:00)
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp parseDateTime(String ts) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date date = simpleDateFormat.parse(ts);
		return new Timestamp(date.getTime());
	}

	/**
	 * Parse date as a string to Timestamp
	 * 
	 * @param ts
	 *            (e.g: 2019-01-30)
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp parseDate(String ts) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = simpleDateFormat.parse(ts);
		return new Timestamp(date.getTime());
	}
}
