package com.egs.common.utils;

public final class StringUtils {

	private StringUtils() {
	}

	public static String join(String seperator, String[] arr) {
		String ret = "";

		if (arr == null || arr.length == 0) {
			return null;
		}

		if (seperator == null || "".equals(seperator)) {
			seperator = ",";
		}

		ret = String.join(seperator, arr);

		return ret;
	}
}
