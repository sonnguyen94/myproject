package com.egs.common.logging.strategy;

import com.egs.common.logging.LogService;

/**
 * 
 * @author Son Nguyen
 *
 * @param <T>
 */
public interface LogStrategy<T> extends LogService {

	@Override
	T getLogger();
}
