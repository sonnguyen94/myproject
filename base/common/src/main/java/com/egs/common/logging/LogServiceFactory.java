package com.egs.common.logging;

import java.util.function.Function;

import com.egs.common.logging.strategy.Log4JStrategy;

/**
 * 
 * @author Son Nguyen
 *
 */
public final class LogServiceFactory {

	/**
	 *
	 */
	private static final Function<String, LogService> createFromString;

	/**
	 *
	 */
	private static final Function<Class<?>, LogService> createFromClass;

	static {
		// default creation method
		createFromString = name -> new Log4JStrategy(name);
		createFromClass = clazz -> new Log4JStrategy(clazz);
	}

	private LogServiceFactory() {
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public static LogService create(String name) {
		return createFromString.apply(name);
	}

	/**
	 *
	 * @param clazz
	 * @return
	 */
	public static LogService create(Class<?> clazz) {
		return createFromClass.apply(clazz);
	}
}
