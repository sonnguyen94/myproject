package com.egs.common.logging;

import org.springframework.boot.logging.LogLevel;

/**
 * 
 * @author Son Nguyen
 *
 */
public interface LogService {
	public static final String METHOD_ENTER = " -> ";

	public static final String METHOD_EXIT = " <- ";

	public static final String METHOD_SEPARATOR = " : ";

	/**
	 *
	 * @return the actual logger used for logging
	 */
	Object getLogger();

	/**
	 *
	 * @param level
	 *            log level
	 * @return true if logging at given level is enabled. Otherwise false.
	 */
	boolean canLog(LogLevel level);

	/**
	 *
	 * @param methodName
	 * @param methodParams
	 */
	void entering(String methodName, Object... methodParams);

	/**
	 * log a message when entering a method
	 *
	 * @param methodName
	 *            name of the method
	 * @param messageFormat
	 * @param messageParams
	 */
	void enteringMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 */
	void exiting(String methodName);

	/**
	 * log a message when exiting a method
	 *
	 * @param methodName
	 * @param messageFormat
	 * @param messageParams
	 */
	void exitingMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 * @param methodParams
	 */
	void debug(String methodName, Object... methodParams);

	/**
	 *
	 * @param methodName
	 * @param messageFormat
	 * @param messageParams
	 */
	void debugMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 * @param t
	 */
	void debugThrowable(String methodName, Throwable t);

	/**
	 *
	 * @param methodName
	 * @param methodParams
	 */
	void info(String methodName, Object... methodParams);

	/**
	 *
	 * @param methodName
	 * @param messageFormat
	 * @param messageParams
	 */
	void infoMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 * @param t
	 */
	void infoThrowable(String methodName, Throwable t);

	/**
	 *
	 * @param methodName
	 * @param methodParams
	 */
	void warn(String methodName, Object... methodParams);

	/**
	 *
	 * @param methodName
	 * @param messageFormat
	 * @param messageParams
	 */
	void warnMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 * @param t
	 */
	void warnThrowable(String methodName, Throwable t);

	/**
	 *
	 * @param methodName
	 * @param methodParams
	 */
	void error(String methodName, Object... methodParams);

	/**
	 *
	 * @param methodName
	 * @param messageFormat
	 * @param messageParams
	 */
	void errorMessage(String methodName, String messageFormat, Object... messageParams);

	/**
	 *
	 * @param methodName
	 * @param t
	 */
	void errorThrowable(String methodName, Throwable t);
}
