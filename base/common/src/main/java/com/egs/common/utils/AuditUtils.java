package com.egs.common.utils;

import java.sql.Timestamp;

import com.egs.common.entity.BaseEntity;

public final class AuditUtils {

	private AuditUtils() {
	}

	public static <T extends BaseEntity> T updateAuditInfo(T t, String auditor, boolean isNewRecord) {
		Timestamp currentTime = TimeUtils.getCurrentTimestamp();
		t.setLastModifiedBy(auditor);
		t.setLastModifiedAt(currentTime);
		if (isNewRecord) {
			t.setCreateAt(currentTime);
		}

		return t;
	}
}
