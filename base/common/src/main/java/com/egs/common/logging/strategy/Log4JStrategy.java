package com.egs.common.logging.strategy;

import java.text.FieldPosition;
import java.text.MessageFormat;
import java.util.HashMap;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.logging.LogLevel;

/**
 * Default logging strategy is log4j
 * 
 * @author Son Nguyen
 *
 */
public class Log4JStrategy extends LogStrategyBase<Logger> {

	private static final HashMap<LogLevel, Level> levelMapping;

	static {
		levelMapping = new HashMap<LogLevel, Level>();
		levelMapping.put(LogLevel.DEBUG, Level.DEBUG);
		levelMapping.put(LogLevel.ERROR, Level.ERROR);
		levelMapping.put(LogLevel.FATAL, Level.FATAL);
		levelMapping.put(LogLevel.INFO, Level.INFO);
		levelMapping.put(LogLevel.OFF, Level.OFF);
		levelMapping.put(LogLevel.TRACE, Level.TRACE);
		levelMapping.put(LogLevel.WARN, Level.WARN);
	}

	/**
	 *
	 * @param logName
	 */
	public Log4JStrategy(String logName) {
		logger = LogManager.getLogger(logName);
	}

	/**
	 *
	 * @param clazz
	 */
	public Log4JStrategy(Class<?> clazz) {
		logger = LogManager.getLogger(clazz);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canLog(LogLevel level) {
		return logger.isEnabled(levelMapping.get(level));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void log(LogLevel level, String methodName, String separator, Object... methodParams) {
		StringBuffer sb = new StringBuffer();
		sb.append(methodName).append(separator);
		if (methodParams != null && methodParams.length > 0) {
			for (Object methodParam : methodParams) {
				sb.append(methodParam);
			}
		}

		logger.log(levelMapping.get(level), sb.toString());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void logThrowable(LogLevel level, String methodName, String separator, Throwable t) {
		logger.log(levelMapping.get(level), methodName + separator, t);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void logMessage(LogLevel level, String methodName, String separator, String messageFormat,
			Object... messageParams) {
		StringBuffer sb = new StringBuffer();
		sb.append(methodName).append(separator);

		// Append formatted message to our StringBuffer!
		MessageFormat format = new MessageFormat(messageFormat);
		format.format(messageParams, sb, new FieldPosition(0));

		logger.log(levelMapping.get(level), sb.toString());
	}
}
