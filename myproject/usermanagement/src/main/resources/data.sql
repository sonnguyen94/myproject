drop table if exists oauth_client_details;
create table oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

drop table if exists oauth_client_token;
create table oauth_client_token (
	token_id VARCHAR(255),
	token bytea ,
	authentication_id VARCHAR(255),
	user_name VARCHAR(255),
	client_id VARCHAR(255)
);

drop table if exists oauth_access_token;
create table oauth_access_token (
	token_id VARCHAR(255),
	token bytea,
	authentication_id VARCHAR(255),
	user_name VARCHAR(255),
	client_id VARCHAR(255),
	authentication bytea,
	refresh_token VARCHAR(255)
);

create index oauth_access_token_id on oauth_access_token(token_id);
create index oauth_refresh_token_id on oauth_access_token(token_id);

drop table if exists oauth_refresh_token;
create table oauth_refresh_token (
	token_id VARCHAR(255),
	token bytea,
	authentication bytea
);
drop table if exists oauth_code;
create table oauth_code (
	code VARCHAR(255),
	authentication bytea
);

drop table if exists oauth_approvals;
create table oauth_approvals (
	userId VARCHAR(255),
	clientId VARCHAR(255),
	scope VARCHAR(255),
	status VARCHAR(10),
	expiresAt TIMESTAMP,
	lastModifiedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO role (id, role_name, description) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin rights');
INSERT INTO role (id, role_name, description) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');

-- non-encrypted password: 123456
INSERT INTO auth_user (id, first_name, last_name, password, user_name, email, version) VALUES (1, 'John', 'Doe', '$2b$10$eBh6roGzQ6Ap/FclnRl1VuTjGfqKWfKnJMcxugejgvMcKIv40tYbC', 'snb', 'user@gmail.com', 0);
INSERT INTO auth_user (id, first_name, last_name, password, user_name, email, version) VALUES (2, 'Admin', 'Admin', '$2b$10$eBh6roGzQ6Ap/FclnRl1VuTjGfqKWfKnJMcxugejgvMcKIv40tYbC', 'admin', 'admin@gmail.com', 0);


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);

-- insert client details
INSERT INTO oauth_client_details
   (client_id, client_secret, scope, authorized_grant_types,
   authorities, access_token_validity, refresh_token_validity)
VALUES
   ('app-client', '$2b$10$eBh6roGzQ6Ap/FclnRl1VuTjGfqKWfKnJMcxugejgvMcKIv40tYbC', 'read,write', 'password,refresh_token,client_credentials,authorization_code', 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 900, 2592000);
INSERT INTO oauth_client_details
   (client_id, client_secret, scope, authorized_grant_types,
   authorities, access_token_validity, refresh_token_validity)
VALUES
   ('ANDROID_APP', '$2b$10$eBh6roGzQ6Ap/FclnRl1VuTjGfqKWfKnJMcxugejgvMcKIv40tYbC', 'read,write', 'password,refresh_token,client_credentials,authorization_code', 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 6048800, 259200000);
ALTER TABLE oauth_access_token ADD CONSTRAINT user_client_const UNIQUE (user_name,client_id);

SELECT setval('role_id_seq', (SELECT MAX(id) from "role"));
SELECT setval('auth_user_id_seq', (SELECT MAX(id) from "auth_user"));

---LDAP username ben Password benspassword