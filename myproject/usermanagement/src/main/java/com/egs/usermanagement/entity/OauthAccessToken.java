package com.egs.usermanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_access_token")
public class OauthAccessToken implements Serializable {

	@Id
	@Column(name = "token_id")
	private String tokenId;

	@Column(name = "token")
	private byte token;

	@Column(name = "authentication_id")
	private String authentication_id;

	@Id
	@Column(name = "user_name")
	private String userName;

	@Id
	@Column(name = "client_id")
	private String clientId;

	@Column(name = "authentication")
	private byte authentication;

	@Column(name = "refresh_token")
	private String refreshToken;

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public byte getToken() {
		return token;
	}

	public void setToken(byte token) {
		this.token = token;
	}

	public String getAuthentication_id() {
		return authentication_id;
	}

	public void setAuthentication_id(String authentication_id) {
		this.authentication_id = authentication_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public byte getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte authentication) {
		this.authentication = authentication;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

}
