package com.egs.usermanagement.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egs.common.logging.LogService;
import com.egs.common.logging.LogServiceFactory;
import com.egs.usermanagement.converter.UserConverter;
import com.egs.usermanagement.entity.Role;
import com.egs.usermanagement.entity.User;
import com.egs.usermanagement.service.RoleService;
import com.egs.usermanagement.service.UserService;
import com.egs.usermanagement.vo.UserVO;

@RestController
@RequestMapping("/users")
public class UserController {

	private static final LogService logger = LogServiceFactory.create(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@GetMapping
	public List<UserVO> getUsers() {
		List<UserVO> ret = new ArrayList<>();

		List<User> users = userService.findAllUsers();
		for (User user : users) {
			ret.add(UserConverter.convertToVO(user));
		}

		return ret;
	}

	@PreAuthorize("hasAuthority('ADMIN_USER')")
	@PostMapping
	public UserVO createUser(@RequestBody UserVO vo) {
		UserVO ret = null;
		User user = UserConverter.convertToEntity(vo);
		List<Role> roles = new ArrayList<>();
		user.setRoles(roles);

		List<String> roleNames = vo.getRoles();
		if (roleNames == null || roleNames.isEmpty()) {
			Role userRole = roleService.findByName("STANDARD_USER");
			roles.add(userRole);
		} else {
			for (String roleName : roleNames) {
				Role role = roleService.findByName(roleName);
				if (role != null) {
					roles.add(role);
				}
			}
		}

		user.setRoles(roles);
		user = userService.createUser(user);

		ret = UserConverter.convertToVO(user);
		return ret;
	}

}
