package com.egs.usermanagement.service;

import com.egs.usermanagement.entity.Role;

public interface RoleService {

	Role findByName(String roleName);

}
