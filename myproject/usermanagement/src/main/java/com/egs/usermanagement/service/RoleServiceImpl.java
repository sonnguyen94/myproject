package com.egs.usermanagement.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.egs.usermanagement.entity.Role;
import com.egs.usermanagement.repository.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role findByName(String roleName) {
		Optional<Role> role = roleRepository.findByRoleName(roleName);
		if (role == null || !role.isPresent()) {
			return null;
		}

		return role.get();
	}

}
