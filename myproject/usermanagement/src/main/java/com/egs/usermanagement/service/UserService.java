package com.egs.usermanagement.service;

import java.util.List;

import com.egs.usermanagement.entity.User;

public interface UserService {
	User findByUsername(String username);

	List<User> findAllUsers();

	User createUser(User user);

	void deleteAll();

}
