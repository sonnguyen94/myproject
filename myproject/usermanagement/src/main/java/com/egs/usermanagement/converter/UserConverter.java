package com.egs.usermanagement.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.egs.usermanagement.entity.Role;
import com.egs.usermanagement.entity.User;
import com.egs.usermanagement.vo.UserVO;

public final class UserConverter {

	private UserConverter() {
	}

	public static User convertToEntity(UserVO vo) {
		User ret = new User();
		ret.setEmail(vo.getEmail());
		ret.setFirstName(vo.getFirstName());
		ret.setLastName(vo.getLastName());
		ret.setPassword(new BCryptPasswordEncoder().encode(vo.getPassword()));
		ret.setUsername(vo.getUsername());

		return ret;
	}

	public static UserVO convertToVO(User user) {
		UserVO ret = new UserVO();
		ret.setEmail(user.getEmail());
		ret.setFirstName(user.getFirstName());
		ret.setLastName(user.getLastName());
		ret.setPassword(user.getPassword());
		ret.setUsername(user.getUsername());

		List<String> roleNames = new ArrayList<String>();
		for (Role role : user.getRoles()) {
			roleNames.add(role.getRoleName());
		}
		ret.setRoles(roleNames);
		ret.setCreateAt(user.getCreateAt());
		ret.setLastModifiedAt(user.getLastModifiedAt());
		ret.setLastModifiedBy(user.getLastModifiedBy());

		return ret;
	}
}
