package com.egs.usermanagement.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.egs.common.logging.LogService;
import com.egs.common.logging.LogServiceFactory;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * Utility class will be used for generating a JWT after a user logs in
 * successfully, and validating the JWT sent in the Authorization header of the
 * requests.
 * 
 * @author Son Nguyen
 *
 */
@Component
public class JwtTokenProvider {

	private static final LogService logger = LogServiceFactory.create(JwtTokenProvider.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;

	public String generateToken(Authentication authentication) {

		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

		return Jwts.builder().setSubject(Long.toString(userPrincipal.getId())).setIssuedAt(new Date())
				.setExpiration(expiryDate).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	public Long getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

		return Long.parseLong(claims.getSubject());
	}

	public boolean validateToken(String authToken) {

		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (MalformedJwtException ex) {
			logger.error("Invalid JWT token: ", ex);
		} catch (ExpiredJwtException ex) {
			logger.error("Expired JWT token: ", ex);
		} catch (UnsupportedJwtException ex) {
			logger.error("Unsupported JWT token: ", ex);
		} catch (IllegalArgumentException ex) {
			logger.error("JWT claims string is empty: ", ex);
		}

		return false;
	}
}
