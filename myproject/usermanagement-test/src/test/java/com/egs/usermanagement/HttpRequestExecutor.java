package com.egs.usermanagement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpRequestExecutor {

	final String baseUrl = "http://localhost:8001/api/v1";

	public HttpRequestExecutor() {
	}

	public ResponseResult executeGet(String url, String accessToken) throws IOException {
		final Map<String, String> headers = new HashMap<>();
		headers.put("Accept", "application/json");
		headers.put("Authorization", "Bearer " + accessToken);
		final HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(headers);
		final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.setErrorHandler(errorHandler);
		ResponseResult responseResult = restTemplate.execute(baseUrl + url, HttpMethod.GET, requestCallback,
				response -> {
					if (errorHandler.hadError) {
						return (errorHandler.getResults());
					} else {
						return (new ResponseResult(response));
					}
				});

		return responseResult;
	}

	public ResponseResult executePost(String url, String body, String accessToken) throws IOException {
		final Map<String, String> headers = new HashMap<>();
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Bearer " + accessToken);
		HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(headers);
		final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

		RestTemplate restTemplate = new RestTemplate();

		requestCallback.setBody(body);
		restTemplate.setErrorHandler(errorHandler);
		ResponseResult responseResult = restTemplate.execute(baseUrl + url, HttpMethod.POST, requestCallback,
				response -> {
					if (errorHandler.hadError) {
						return (errorHandler.getResults());
					} else {
						return (new ResponseResult(response));
					}
				});

		return responseResult;
	}

	private static class ResponseResultErrorHandler implements ResponseErrorHandler {
		private ResponseResult result = null;
		private Boolean hadError = false;

		private ResponseResult getResults() {
			return result;
		}

		@Override
		public boolean hasError(ClientHttpResponse response) throws IOException {
			hadError = response.getRawStatusCode() >= 400;
			return hadError;
		}

		@Override
		public void handleError(ClientHttpResponse response) throws IOException {
			result = new ResponseResult(response);
		}
	}

	public ResponseResult executeLogin() {
		final Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		headers.put("Authorization", "Basic YXBwLWNsaWVudDoxMjM0NTY=");

		MultiValueMap<String, String> formData = new LinkedMultiValueMap<String, String>();
		formData.add("grant_type", "password");
		formData.add("username", "admin");
		formData.add("password", "123456");
		RequestCallback request = new OAuth2AuthTokenCallback(formData, headers);
		final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.setErrorHandler(errorHandler);
		ResponseResult responseResult = restTemplate.execute(baseUrl + "/oauth/token", HttpMethod.POST, request,
				response -> {
					if (errorHandler.hadError) {
						ResponseResult ret = errorHandler.getResults();
						return ret;
					} else {
						return (new ResponseResult(response));
					}
				});

		return responseResult;
	}
}
