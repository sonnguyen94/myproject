package com.egs.usermanagement;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(classes = { UserMgmtApplication.class }, webEnvironment = WebEnvironment.DEFINED_PORT)
public class CucumberSpringContextConfiguration {

}
